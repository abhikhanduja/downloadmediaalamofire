//
//  ViewController.swift
//  DownloadingMediaAlamofire
//
//  Created by abhinav khanduja on 17/11/19.
//  Copyright © 2019 abhinav khanduja. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let meds = DBRef.ref.getAllMediaPaths()
        meds.forEach({print($0.name)})
//        downloadVideo()
    }

    func downloadVideo(){
        
        Alamofire.request("https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4").downloadProgress(closure : { (progress) in
//            print(progress.fractionCompleted)
        }).responseData{ (response) in
//            print(response)
//            print(response.result.value!)
//            print(response.result.description)
            if let data = response.result.value {
                let fileManager = FileManager.default
                let documentsURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
                let name = "video.mp4"
                let videoURL = documentsURL.appendingPathComponent(name)
                
                if fileManager.fileExists(atPath: videoURL.path) {
                    print("file already exists")
                    return
                }
                
                do {
                    try data.write(to: videoURL)
                    DBRef.ref.saveMedia(type: .video, name: name, path: videoURL.path)
                } catch {
                    print("Something went wrong!")
                }
                print("video url is: ",videoURL)
            }
        }
    }

}

